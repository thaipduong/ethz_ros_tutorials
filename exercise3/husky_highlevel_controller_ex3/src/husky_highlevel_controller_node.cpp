#include <ros/ros.h>

#include "husky_highlevel_controller/HuskyHighlevelController.hpp"



int main(int argc, char** argv)
{
  ros::init(argc, argv, "husky_highlevel_controller_ex2");
  ros::NodeHandle nodeHandle("~");
  husky_highlevel_controller::HuskyHighlevelController huskyHighlevelController(nodeHandle);

  ros::spin();
  return 0;
}
