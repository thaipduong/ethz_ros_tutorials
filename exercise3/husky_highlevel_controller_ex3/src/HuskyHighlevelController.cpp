#include "husky_highlevel_controller/HuskyHighlevelController.hpp"

namespace husky_highlevel_controller {

HuskyHighlevelController::HuskyHighlevelController(ros::NodeHandle& nodeHandle) :
  nodeHandle_(nodeHandle)
{
    this->getParams(nodeHandle);
    ros::Subscriber subscriber = nodeHandle.subscribe(this->topicName, this->queueSize, &HuskyHighlevelController::scanCallback, this);
}

HuskyHighlevelController::~HuskyHighlevelController()
{
}

void HuskyHighlevelController::scanCallback(const sensor_msgs::LaserScan& msg)
{
//ROS_INFO("I heard: [%s]", msg.data.c_str());
  float min = msg.range_max;
  for (int i=0; i<msg.ranges.size(); i++)
  {
    if (min > msg.ranges[i] && msg.range_min < msg.ranges[i])
      min = msg.ranges[i];
  }
  ROS_INFO("Distance: [%f]\n", min);
}

void HuskyHighlevelController::getParams(ros::NodeHandle& nodeHandle)
{
  if (!nodeHandle.getParam("topic_name", this->topicName)) {
  ROS_ERROR("Could not find topic parameter!");
  }
  else {
    ROS_INFO("Topic Name: [%s]\n", this->topicName.c_str());
  }

  if (!nodeHandle.getParam("queue_size", this->queueSize)) {
    ROS_ERROR("Could not find queue size parameter!");
  }
  else {
    ROS_INFO("Queue Size: [%d]\n", this->queueSize);
  }
}

} /* namespace */
